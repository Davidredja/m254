![Logo ZH](./x_gitressourcen/ZH_logo.jpg)
# M254 Geschäftsprozesse im eigenen Berufsumfeld beschreiben

Die Lernenden führen Aufträge aus dem eigenen Berufsumfeld gemäss Vorgaben des Auftraggebers selbständig und mit Hilfe geeigneter Techniken, Methoden und Hilfsmittel durch.

- In der [Modulidentifikation M254](https://www.modulbaukasten.ch/module/254) finden Sie die Vorgaben von ICT zum Modul
- Die Bewertung des Moduls basiert auf dem [M254 Kompetenzraster TBZ](./Kompetenzraster.md)
- Auf dem [Campus](https://tbzedu.sharepoint.com/:f:/s/campus/students/Ela-NizmRAdOi9tOrdHaVasBBBQX11KE0yhP_kpreDD5Yw?e=alEW3c) finden Sie Unterlagen, welche aus urheberrechtlichen Gründen nicht öffentlich einsehbar sein dürfen.
- Im Unterricht wird Camunda als Workflow-Engine verwendet.