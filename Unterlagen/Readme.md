# Unterlagen

## Camunda

Im Unterricht wird mit der Workflow-Engine von Camunda gearbeitet. Der Code ist Open Source und eignet sich daher sehr gut für schulische Experimente.

Aktuell gibt es Camunda in der Version 8. Diese baut auf Microservices auf und ist damit grundlegend anders aufgebaut als die früheren Versionen. Sie ist auch als SaaS verfügbar.

Im Unterricht wird aber mehrheitlich mit Version 7 gearbeitet. 

Weitere Details zu Installationen, Anwendungsbeispielen, etc.:
- [Camunda7](./Camunda7/Readme.md
- [Camunda8](./Camunda8/Readme.md