# Camunda Version 8

## Installation

Die Docker-Installation benötigt nur gerade drei Schritte:
1) Download des Git-Repository
```
git clone https://github.com/camunda/camunda-platform/tree/main
```

2) Wechseln in das neue Verzeichnis.
```
cd camunda-platform
```

3) Aufruf Docker compose
```
docker compose up -d
```

## Modeler

Modeler
Für die Arbeit mit Camunda8 muss die neuste Modeler-Version heruntergeladen werden. 
Die REST-Engine kann unter http://localhost:26500 erreicht werden. Als Target muss “Camunda 8 Self-Managed” gewählt werden.

![](./x_gitressourcen/Deploy_Self-Managed.jpg)

### Access-Tokens

Bei Zugriffen über das REST-API wird die Identität überprüft. 
[Hier](https://docs.camunda.io/docs/self-managed/operate-deployment/operate-authentication/#identity) gibt es Informationen dazu. 

### Web Modeler API
REST-API für Entwickler mit Zugriff auf Dateiebene ⇒ siehe [Überblick](https://camunda.com/blog/2023/10/public-rest-api-web-modeler/)

### Swagger-UI

Swagger lokal: http://localhost:8081/swagger-ui/index.html
Wichtig: Zuerst Login unter http://localhost:8081/swagger-ui ausführen, sonst gibt es Fehler “Failed to load remote configuration”:
![](./x_gitressourcen/Swagger_Failure-Failed_to_load_remote_configuration.jpg)