# Signal ausgeben und empfangen

## Aufgabenstellung

Das Auslösen eines Signal-Events und die Reaktion darauf soll anhand eines Beispiels gezeigt werden.

## Angaben zur Lösung

Im BPMN sind **drei** Prozesse definiert. Der erste und letzte Prozess kann ausgeführt werden. Der zweite Prozess startet als Reaktion auf einen Signaleingang.

### Ablauf/Vorgehen

1) Prozess "Signal-Test: Prozess mit empfang. Signal-Zwischenereignis" aufrufen und ersten "UserTask" completen. Der Prozess wartet danach auf ein Signal und ist daher in der Tasklist nicht mehr sichtbar.
2) Prozess "Signal-Test: Signal ausgeben" aufrufen und ersten "UserTask" completen. Der Prozess gibt darauf ein Signal aus.
3) Es sollten nun zwei UserTasks aus zwei Prozessinstanzen in der Tasklist sichtbar sein:
a) Der zweite Prozess wurde durch das Signal gestartet und steht beim UserTask "Signal erhalten".
b) Der dritte Prozess hat das Signal erhalten und steht nun beim UserTask "KontrollTask".
4) Die beiden Tasks können completed werden. 