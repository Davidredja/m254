# pip install camunda-external-task-client-python3
# wichtig: pip install camunda-external-task-client-python3
from camunda.external_task.external_task_worker import ExternalTaskWorker

# Konfigurieren Sie den External Task Worker
worker = ExternalTaskWorker('http://localhost:8080/engine-rest', 'workerId')

# Die Arbeitslogik für den "DoWork"-Auftrag
def dowork_task_handler(task, taskservice):
    # Hier können Sie die Arbeit für den "DoWork"-Auftrag durchführen
    # task enthält die Daten vom Camunda-Prozess
    print(f'Arbeite an Aufgabe {task.id}')
    # Beispiel: Markiere die Aufgabe als erledigt
    taskservice.complete(task)

# Registrieren Sie den Aufgaben-Handler
worker.handler = dowork_task_handler

worker.subscribe('DoWork')  # Abonnement auf das Topic "DoWork"

# Starten Sie den External Task Worker
worker.start()
