Zeigt einen Eventbased-Gateway mit zwei Pfaden:
- Bedingung (Expression)
- Timer auf 1Sek

Wenn als Location "Paris" ausgewählt wird, spricht die Bedingung an.
In allen anderen Fällen wird nach 1S der andere Pfad verfolgt.