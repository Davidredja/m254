# Prozess aus PowerShell heraus starten

## Aufgabenstellung

Ein Prozess in Camunda soll aus einem PowerShell-Skript heraus gestartet werden.

## Angaben zur Lösung

1) Deployen Sie die BPMN-Datei [PSProzess.bpmn](PSProzess.bpmn).
2) Starten Sie das [StartCamundaProcess.ps1](StartCamundaProcess.ps1) mit PowerShell
3) Vergewissern Sie sich, dass in Camunda eine neue Prozessinstanz erstellt wurde.