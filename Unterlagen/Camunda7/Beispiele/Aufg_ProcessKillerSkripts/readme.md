# ProcessKiller

## Aufgabenstellung

Es soll (bspw. mit Python/PowerShell) ein Skript erstellt werden, über welches alle laufenden Prozessinstanzen aus dem System gelöscht werden.

## Angaben zur Lösung

Das Skript benötigt keinen Parameter.
Zuerst wird eine Liste der vorhandenen Prozessinstanzen angefordert. Danach werden diese einzeln gelöscht.
