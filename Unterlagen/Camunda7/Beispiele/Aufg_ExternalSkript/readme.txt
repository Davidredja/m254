Externes Script einbinden
-------------------------

Das Beispiel zeigt, wie ein JS-File eingebunden werden kann und wie man einen Wert daraus zurückgeben kann.

Angabe:
deployment://test.js

Die test.js-Datei muss natürlich als "Additional File" im Deployment mitgegeben werden.

Der Wert, welcher in der Eingabemaske eingegeben wird, wird im Skript-Task im Log ausgegeben und danach wieder in eine andere Variable geschrieben. Diese neue Variable wird im letzten Task angezeigt.