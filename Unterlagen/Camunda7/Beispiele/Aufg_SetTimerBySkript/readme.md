# Timer über eine Variable setzen

## Aufgabenstellung

Die Dauer eines Timer-Tasks soll über eine Variable gesetzt werden.

## Hinweise zur Lösung

Das BPMN mit den zwei Formularen deployen. 
Auf der ersten Maske gibt es zwei Möglichkeiten, die Dauer anzugeben:

1) die Zeit kann in Sekunden erfasst werden (Variable _durationInSeconds_)
2) die Angabe erfolgt gemäss interner Vorgabe von Camunda, bspw. PT15S für 15 Sekunden. Sie wird in der Variablen _duration_ gespeichert. ([Weitere Infos](https://docs.camunda.org/manual/latest/reference/bpmn20/events/timer-events/#time-date) zu dieser Angabe)

Beim ersten Task ist ein End-Listener angehängt. In diesem wird in einem JavaScript die Variable _duration_ gefüllt, falls diese Angabe nicht direkt im Formular gefüllt wurde.


``` javascript
let duration = execution.getVariable("duration");

if (!duration) { // Dies prüft auf "", null, undefined, 0, false, und NaN
  let durationInSeconds = execution.getVariable("durationInSeconds");
  execution.setVariable("duration", "PT" + durationInSeconds + "S");
}

console.log("*** Verwendete 'duration':" + execution.getVariable("duration"));
```
