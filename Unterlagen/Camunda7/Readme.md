# Camunda Version 7


## Installation

**Voraussetzung**: Docker (oder Docker Desktop) ist installiert.

Für die Installation gehen Sie wie folgt vor:

1) Download des Git-Repository 
```
docker pull camunda/camunda-bpm-platform:run-latest
```

1) Starten eines Docker-Containers
```
docker run -d --name camundaV7 -p 8080:8080 camunda/camunda-bpm-platform:run-latest
```
1) Browser öffnen mit der Url [http://localhost:8080](http://localhost:8080) (es dauert evtl. etwas, bis die Camunda-Umgebung gestartet ist). 
2) Login mit User "demo" und Passwort "demo"


Weitere Möglichkeiten siehe [hier](https://docs.camunda.org/get-started/quick-start/install/)

## REST-API

Die Camunda-Engine bietet ein REST-API an. Mittels REST-Aufrufen kann Camunda nach Informationen über Prozesse, Tasks, etc. gefragt werden. 

Aufruf Swagger: http://localhost:8080/swaggerui/#

## Prozess-Beispiele

Im Ordner [Beispiele](./Beispiele/) finden Sie diverse kleine Prozesse, welche die Funktionsweise von BPMN-Symbolen zeigen.

Ausserdem bietet Camunda auf [GitHub](https://github.com/camunda/camunda-bpm-examples) ein grosse Anzahl von Beispielen an.